# LanthanumGraphJS
 
LanthanumGraphJS est un outil pour réaliser des graphiques simples. Vous pouvez accéder à la démo via ce [lien](https://lanthanes.gitlab.io/LanthanumGraphJS/).
 
## Type de graphique
 
L'outil fournie pour le moment que deux types de graphique :
- Ligne
- Point

```javascript 
const TypeGraph = {
    LINE:   0,
    POINT:  1,
};
```

## Prérequis
 
Cet outil utilise des paramètres en JSON pour être configuré. Voici une liste des options pouvant être modifié :
 
| Option | Défaut | Description |
| --- | --- | --- |
| `xMargin`             | 50                | Marge à gauche |
| `yMargin`             | 25                | Marge en bas |
| `topMargin`           | 20                | Marge au sommet |
| `rightMargin`         | 20                | Marge à droite |
| `fontStyle`           | Arial             | Police d'écriture |
| `backgroundColor`     | #fff              | Couleur d'arrière plan |
| `colorScale`          | #000              | Couleur des barres abscisse\ordonnée |
| `colorText`           | #000              | Couleur du texte |
| `scaleRate`           | 0.1               | Ordre d'échelle du graphique |
| `animate`             | true              | Active\Désactive l'animation (rendu plus fluide) |
| `multipleX`           | 5                 | Donne le *pas* d'affichage au curseur sur l'axe X |
| `multipleY`           | 5                 | Donne le *pas* d'affichage au curseur sur l'axe Y |
| `lineWidth`           | 2                 | Taille des lignes |
| `circleSize`          | 3                 | Taille des cercles |
| `typeGraph`           | TypeGraph.LINE    | Type de graphique à afficher |
| `arround`             | 2                 | Définie le nombre de chiffres après |
 
## Contenu
 
Voici le code javascript qui contient les données de configuration.
 
```javascript 
    // CONTIENT LES DONNEES A AFFICHER
    this.datasets = {
        axis: [{
            x: [],                      // array of values
            color: '#000',              // color of line\points...
        }],
        y: {
            labels: []
        },
    };
    // OPTIONS DE CONFIGURATION
    this.options = {
        size: {
            width: 1000,
            height: 250,
        },
        config: {
            xMargin: 50,                // minimum value => 0
            yMargin: 25,                // minimum value => 0
            topMargin: 20,              // minimum value => 0
            rightMargin: 20,            // minimum value => 0
            fontStyle: 'Arial',
            backgroundColor: '#fff',
            colorScale: '#000',
            colorDottedScale: '#000',
            colorText: '#000',
            scaleWidth: 0.6,            // minimum value => 0.1
            scaleRate: 0.1,             // minimum value => 0.1
            animate: true,
            timer: 20,
            multipleX: 1,               // minimum value => 1
            multipleY: 1,               // minimum value => 1
            resizeAuto: true,
            lineWidth: 2,               // minimum value => 1
            circleSize: 3,              // minimum value => 0
            typeGraph: TypeGraph.LINE,
            arround: 2,
        },
    }; 
```
 
## Comment démarrer
 
Voici, en quelques lignes, le code pour mettre en place le graphique avec des données. La fonction **LTH_GraphEngine** retourne une ***Promise***.
 
```javascript 
var ctx = document.querySelector('#canvas-raph').getContext('2d');
 
var data = new Data();
/**
 * Remplacer les '???' par les options\valeurs nécessaires
 *
 * data.options.config.???
 * data.options.size.???
 * data.datasets.axis.???
 */
 
data.datasets.axis = [
    {
        x: [121, 268, 189, 325],
        color: "green",
    },
    {
        x: [80, 122, 78, 111],
        color: "red",
    },
];
data.datasets.y.labels = ["Janvier", "Février", "Mars", "Avril"];
 
LTH_GraphEngine(ctx, data); 
```
📈

## Auteur

- Romain Dufaure - [Site Web](https://romain-dufaure.fr/)
