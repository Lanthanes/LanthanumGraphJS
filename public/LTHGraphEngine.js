/**
 * 
 * CChartJS v1.0
 * by Romain Dufaure
 * 
 */

const TypeGraph = {
    LINE:   0,
    POINT:  1,
    BAR:    2,
};
    
const config = {
    length: 0,
    yMax: 0,
    yMin: 0,
    yScaleMin: 0,
    yScaleMax: 0,
    autoScaleRate: 0,
    scaleX: 0,
    scaleY: 0,
    xRate: 0,
    yRate: 0,
    endOfDraw: true,
    progress: false,
}

/** 
 * 
 * @param {CanvasRenderingContext2D} ctx 
 * @param {Data} data 
 * @returns 
 */
function LTH_GraphEngine(ctx, data) {
    
    let autoconfig = config;

    /** 
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {Data} data 
     * @returns 
     */
    async function Run(ctx, data) {
        
        if(autoconfig.progress) throw new Error("ERROR: Simulation has already been launched !!!");
        if(ctx === 'undefined' || ctx == null) throw new Error("ERROR: Canvas context is empty !!!");
        if(data === 'undefined' || data == null) throw new Error("ERROR: Data is empty !!!");

        ctx.canvas.removeEventListener("mousemove", MouseMoveCanvas);
        ctx.canvas.removeEventListener("mouseleave", MouseLeaveCanvas);
    
        autoconfig.progress = true;
        autoconfig.endOfDraw = false;
    
        ctx.canvas.width = data.options.size.width;
        ctx.canvas.height = data.options.size.height;

        Reset();
        
        autoconfig.scaleX = ctx.canvas.clientWidth / ctx.canvas.width;
        autoconfig.scaleY = ctx.canvas.clientHeight / ctx.canvas.height;
        autoconfig.length = data.datasets.y.labels.length;
    
        // CHECK VALUE ERROR
        data.options.config.arround = Math.ceil(data.options.config.arround);
        data.options.config.yScale = Math.ceil(data.options.config.yScale);

        if(data.options.config.xMargin < 0) data.options.config.xMargin = 0;
        if(data.options.config.yMargin < 0) data.options.config.yMargin = 0;
        if(data.options.config.topMargin < 0) data.options.config.topMargin = 0;
        if(data.options.config.rightMargin < 0) data.options.config.rightMargin = 0;
        if(data.options.config.scaleWidth < 0) data.options.config.scaleWidth = 0.1;
        if(data.options.config.scaleRate < 0) data.options.config.scaleRate = 0.1;
        if(data.options.config.multipleX <= 0) data.options.config.multipleX = 1;
        if(data.options.config.multipleY <= 0) data.options.config.multipleY = 1;
        if(data.options.config.lineWidth <= 0) data.options.config.lineWidth = 1;
        if(data.options.config.circleSize < 0) data.options.config.circleSize = 0;
        if(data.options.config.typeGraph < 0) data.options.config.typeGraph = TypeGraph.LINE;
        if(data.options.config.yScale <= 0) data.options.config.yScale = 1;
        if(data.options.config.arround <= 0) data.options.config.arround = 0;
    
        // return error if size on y != of size on labels
        for (let i = 0; i < data.datasets.axis.length; i++) {
            if (data.datasets.axis[i].x.length != autoconfig.length) {
                throw new Error("ERROR: The size of axes are different of size of labels. Size must be equals !!!");
            }
        }
    
        // GET yMax & yMin VALUE
        for (let y = 0; y < data.datasets.axis.length; y++) {
            for (let x = 0; x < data.datasets.axis[y].x.length; x++) {
    
                if (autoconfig.yMax < data.datasets.axis[y].x[x]) {
                    autoconfig.yMax = data.datasets.axis[y].x[x];
                }
    
                if (autoconfig.yMin > data.datasets.axis[y].x[x]) {
                    autoconfig.yMin = data.datasets.axis[y].x[x];
                }
            }
        }

        // GET yMax & yMin SCALE VALUE
        autoconfig.yScaleMin = ArroundTen(autoconfig.yMin, data.options.config.yScale); // autoconfig.yMin < 0 ? autoconfig.yMin - 1 : autoconfig.yMin +  1;
        autoconfig.yScaleMax = ArroundTen(autoconfig.yMax, data.options.config.yScale); // autoconfig.yMax < 0 ? autoconfig.yMax - 1 : autoconfig.yMax +  1;
    
        autoconfig.autoScaleRate = (Math.abs(autoconfig.yScaleMin) + Math.abs(autoconfig.yScaleMax));
        autoconfig.xRate = (data.options.size.width - data.options.config.xMargin - data.options.config.rightMargin) / (data.datasets.y.labels.length - 1);
        autoconfig.yRate = (data.options.size.height - data.options.config.yMargin - data.options.config.topMargin) / autoconfig.autoScaleRate;

        let axisNumber = 0;
        for (let i = 0; i < data.datasets.axis.length; i++) {
            if (data.datasets.axis[i].x.length > axisNumber) {
                axisNumber = data.datasets.axis[i].x.length
            }            
        }

        // ERROR MANAGER
        if (data.datasets.axis.length <= 0 || axisNumber <= 1) DrawError(ctx, "ERROR: To low data !!!");
        
        Draw(ctx, data, data.options.config.animate);
    
        /**
         * 
         * @param {CanvasRenderingContext2D} ctx 
         * @param {Data} data 
         * @param {Boolean} animate 
         */
        async function Draw(ctx, data, animate = false) {
    
            autoconfig.endOfDraw = false;
    
            // RESET GRAPH
            ctx.clearRect(0, 0, data.options.size.width, data.options.size.height);
            ctx.beginPath();
            
            // DRAW BACKGROUND
            ctx.fillStyle = data.options.config.backgroundColor;
            ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height);
            ctx.fill();
    
            // DRAW LINE X BORDER
            ctx.beginPath();
            ctx.strokeStyle = data.options.config.colorScale;
            ctx.lineWidth = data.options.config.scaleWidth;
            ctx.setLineDash([0, 0]);
            ctx.moveTo(data.options.config.xMargin, 0);
            ctx.lineTo(data.options.config.xMargin, data.options.size.height);
            ctx.stroke();
        
            // DRAW LINE Y BORDER
            ctx.beginPath();
            ctx.strokeStyle = data.options.config.colorScale;
            ctx.lineWidth = data.options.config.scaleWidth;
            ctx.setLineDash([0, 0]);
            ctx.moveTo(data.options.config.xMargin, (data.options.size.height - data.options.config.yMargin));
            ctx.lineTo(data.options.size.width, (data.options.size.height - data.options.config.yMargin));
    
            ctx.font = '12px ' + data.options.config.fontStyle;
            ctx.textAlign = "end";
    
            ctx.stroke();
        
            // DRAW Y SCALE OF ARRAY
            let value = autoconfig.yScaleMax;
            for (let y = 0; y <= autoconfig.autoScaleRate; y += Number(data.options.config.yScale)) {
    
                ctx.beginPath();
                ctx.strokeStyle = data.options.config.colorDottedScale;
                ctx.lineWidth = data.options.config.scaleWidth;

                if (value == 0) ctx.setLineDash([0, 0]);
                else ctx.setLineDash([3, 3]); // => [size of line, size of separation]
    
                ctx.moveTo(data.options.config.xMargin, (y * autoconfig.yRate) + data.options.config.topMargin);
                ctx.lineTo(data.options.size.width, (y * autoconfig.yRate) + data.options.config.topMargin);
    
                ctx.fillStyle = data.options.config.colorText;
                ctx.fillText(value.toFixed(0), (data.options.config.xMargin - 5), (y * autoconfig.yRate) + data.options.config.topMargin + 5);
    
                ctx.stroke();
    
                value -= data.options.config.yScale;
            }
        
            // DRAW X SCALE OF ARRAY
            for (let y = 0; y < autoconfig.length; y += 1) {
            
                ctx.beginPath();
                ctx.strokeStyle = data.options.config.colorDottedScale;
                ctx.lineWidth = data.options.config.scaleWidth;
                ctx.setLineDash([3, 3]); // => [size of line, size of separation]
    
                if(y != 0) {
                    ctx.moveTo((y * autoconfig.xRate) + data.options.config.xMargin, data.options.size.height - data.options.config.yMargin);
                    ctx.lineTo((y * autoconfig.xRate) + data.options.config.xMargin, 0); // data.options.size.height
                }
    
                ctx.textAlign = "center";
                ctx.fillStyle = data.options.config.colorText;
                ctx.fillText(data.datasets.y.labels[y], (y * autoconfig.xRate) + data.options.config.xMargin + (y == 0 ? 5 : 0), data.options.size.height - (data.options.config.yMargin - 15));
        
                ctx.stroke();
            }
    
            // DRAW CURVES
            if (data.options.config.animate && animate) {
                
                for (let line = 0; line < data.datasets.axis.length; line++) {
                    
                    if (data.options.config.typeGraph == TypeGraph.LINE) {
    
                        ctx.beginPath();
                        ctx.strokeStyle = data.datasets.axis[line].color;
                        ctx.lineWidth = data.options.config.lineWidth;
                        ctx.setLineDash([0, 0]);
        
                        ctx.moveTo(
                            (0 + data.options.config.xMargin),
                            (autoconfig.yScaleMax - data.datasets.axis[line].x[0]) * autoconfig.yRate + data.options.config.topMargin
                        ); // TODO set calcul to function
                
                        for (let datasets = 0; datasets < autoconfig.length; datasets++) {
        
                            if(datasets == 0) {
                                ctx.lineTo(
                                    (datasets * autoconfig.xRate + data.options.config.xMargin),
                                    (autoconfig.yScaleMax - data.datasets.axis[line].x[datasets]) * autoconfig.yRate + data.options.config.topMargin
                                ); // TODO set calcul to function (bis)
                            } else {
                                ctx.beginPath();
        
                                ctx.moveTo(
                                    ((datasets - 1) * autoconfig.xRate + data.options.config.xMargin),
                                    (autoconfig.yScaleMax - data.datasets.axis[line].x[datasets - 1]) * autoconfig.yRate + data.options.config.topMargin
                                ); // TODO set calcul to function
                                ctx.lineTo(
                                    (datasets * autoconfig.xRate + data.options.config.xMargin),
                                    (autoconfig.yScaleMax - data.datasets.axis[line].x[datasets]) * autoconfig.yRate + data.options.config.topMargin
                                ); // TODO set calcul to function (bis)
                            }
        
                            ctx.stroke();
    
                            await timer(data.options.config.timer);
                        }
                    }
                    if (data.options.config.circleSize > 0 && (data.options.config.typeGraph == TypeGraph.LINE || data.options.config.typeGraph == TypeGraph.POINT)) {
                
                        for (let datasets = 0; datasets < autoconfig.length; datasets++) {
        
                            ctx.beginPath();
                            ctx.fillStyle = data.datasets.axis[line].color;
                            ctx.arc(
                                (datasets * autoconfig.xRate + data.options.config.xMargin), 
                                (autoconfig.yScaleMax - data.datasets.axis[line].x[datasets]) * autoconfig.yRate + data.options.config.topMargin, 
                                data.options.config.circleSize, 
                                0, 
                                2 * Math.PI, 
                                false
                            );
                            ctx.fill();
    
                            await timer(data.options.config.timer);
                        }
                    }
                }
    
                Draw(ctx, data);

            } else {
                
                for (let line = 0; line < data.datasets.axis.length; line++) {
    
                    if (data.options.config.typeGraph == TypeGraph.LINE) {
            
                        ctx.beginPath();
                        ctx.strokeStyle = data.datasets.axis[line].color;
                        ctx.lineWidth = data.options.config.lineWidth;
                        ctx.setLineDash([0, 0]);
            
                        ctx.moveTo(
                            (0 + data.options.config.xMargin),
                            (autoconfig.yScaleMax - data.datasets.axis[line].x[0]) * autoconfig.yRate + data.options.config.topMargin
                        ); // TODO set calcul to function
                
                        for (let datasets = 0; datasets < autoconfig.length; datasets++) {
                            ctx.lineTo(
                                (datasets * autoconfig.xRate + data.options.config.xMargin),
                                (autoconfig.yScaleMax - data.datasets.axis[line].x[datasets]) * autoconfig.yRate + data.options.config.topMargin
                            ); // TODO set calcul to function (bis)
                        }
                
                        ctx.stroke();
                    }
                    if (data.options.config.circleSize > 0 && (data.options.config.typeGraph == TypeGraph.LINE || data.options.config.typeGraph == TypeGraph.POINT)) {
        
                        for (let datasets = 0; datasets < autoconfig.length; datasets++) {
                            ctx.beginPath();
                            ctx.fillStyle = data.datasets.axis[line].color;
                            ctx.arc(
                                (datasets * autoconfig.xRate + data.options.config.xMargin), 
                                (autoconfig.yScaleMax - data.datasets.axis[line].x[datasets]) * autoconfig.yRate + data.options.config.topMargin, 
                                data.options.config.circleSize, 
                                0, 
                                2 * Math.PI, 
                                false
                            );
                            ctx.fill();                    
                        }
                    }
                }
            }

            autoconfig.endOfDraw = true;
        }
        
        function Resize() {
    
            if(data.options.config.resizeAuto) {
                ctx.canvas.width = ctx.canvas.clientWidth;
        
                Draw(ctx, data);
            }
        }

        function Reset() {

            autoconfig.length = 0;
            autoconfig.yMax = 0;
            autoconfig.yMin = 0;
            autoconfig.yScaleMax = 0;
            autoconfig.yScaleMin = 0;
            autoconfig.autoScaleRate = 0;
            autoconfig.scaleX = 0;
            autoconfig.scaleY = 0;
            autoconfig.xRate = 0;
            autoconfig.yRate = 0;
        }
    
        autoconfig.progress = false;
    
        return {
            resize: Resize,
            draw: Draw,
            data: data,
            conf: autoconfig,
        };
    }

    function DrawError(ctx, message = "ERROR") {
    
        // RESET GRAPH
        ctx.clearRect(0, 0, data.options.size.width, data.options.size.height);
        ctx.beginPath();
        
        // DRAW BACKGROUND
        ctx.fillStyle = data.options.config.backgroundColor;
        ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.fill();

        const sizeBox = [(data.options.size.width / 3), data.options.size.height / 3]
        
        ctx.fillStyle = "#e74c3c";
        ctx.fillRect((data.options.size.width / 2) - (sizeBox[0] / 2), (data.options.size.height / 2) - (sizeBox[1] / 2), sizeBox[0], sizeBox[1]);
        ctx.strokeStyle = "#c0392b";
        ctx.lineWidth = 3;
        ctx.strokeRect((data.options.size.width / 2) - (sizeBox[0] / 2), (data.options.size.height / 2) - (sizeBox[1] / 2), sizeBox[0], sizeBox[1]);    
        ctx.font = 'bold 22px ' + data.options.config.fontStyle;
        ctx.textAlign = "center";
        ctx.fillStyle = "#ecf0f1";
        ctx.fillText(message, (data.options.size.width / 2), (data.options.size.height / 2) + 5);
    
        autoconfig.progress = false;

        throw new Error(message);
    }

    function MouseMoveCanvas(e, autoconfig, data, draw) {

        if(!autoconfig.progress && autoconfig.endOfDraw) {

            var cRect = ctx.canvas.getBoundingClientRect();
            var canvasX = Math.round((e.clientX - cRect.left) * autoconfig.scaleX / data.options.config.multipleX) * data.options.config.multipleX;
            var canvasY = Math.round((e.clientY - cRect.top) * autoconfig.scaleY / data.options.config.multipleY) * data.options.config.multipleY;

            draw(ctx, data);
            
            if(
                canvasX > data.options.config.xMargin && 
                canvasX < (data.options.size.width - data.options.config.rightMargin) && 
                canvasY <= (data.options.size.height - data.options.config.yMargin) &&
                canvasY >= (data.options.config.topMargin)
            ) {
                
                ctx.beginPath();

                ctx.fillStyle = data.options.config.colorText;
                // ctx.fillText(((((data.options.size.height - data.options.config.yMargin) - canvasY) / autoconfig.yRate)).toFixed(data.options.config.arround), canvasX, canvasY);
                ctx.fillText((autoconfig.yScaleMax - ((canvasY - data.options.config.topMargin) / autoconfig.yRate)).toFixed(data.options.config.arround), canvasX, canvasY);

                ctx.strokeStyle = data.options.config.colorScale;
                ctx.lineWidth = data.options.config.scaleWidth;
                ctx.setLineDash([1, 1]); // => [size of line, size of separation]
                
                ctx.moveTo(canvasX, 0);
                ctx.lineTo(canvasX, data.options.size.height - data.options.config.yMargin);
                ctx.stroke();
        
                ctx.beginPath();
                ctx.moveTo(data.options.config.xMargin, canvasY);
                ctx.lineTo(data.options.size.width, canvasY);
                ctx.stroke();
            }
        }
    }

    function MouseLeaveCanvas(_, autoconfig, data, draw) {

        if(!autoconfig.progress && autoconfig.endOfDraw) {

            draw(ctx, data);
        }
    }

    const ArroundTen = (value, scale = 10) => Math.ceil(Math.abs(value) / scale) * scale * (value < 0 ? -1 : 1);
    
    return Run(ctx, data).then(result => {

        ctx.canvas.addEventListener("mousemove", e => MouseMoveCanvas(e, result.conf, result.data, result.draw));
        ctx.canvas.addEventListener("mouseleave", e => MouseLeaveCanvas(e, result.conf, result.data, result.draw));
    
        autoconfig.progress = false;
    
        return {
            resize: result.resize,
            draw: result.draw,
            data: data,
            conf: autoconfig,
        };
    });
}


function Data() {
    this.datasets = {
        axis: [{
            x: [],                          // array of values
            color: '#000',                  // color of line\points...
        }],
        y: {
            labels: []
        },
    };
    this.options = {
        size: {
            width: 1000,
            height: 250,
        },
        config: {
            xMargin: 50,                    // minimum value => 0
            yMargin: 25,                    // minimum value => 0
            topMargin: 20,                  // minimum value => 0
            rightMargin: 20,                // minimum value => 0
            fontStyle: 'Arial',
            backgroundColor: '#fff',
            colorScale: '#000',
            colorDottedScale: '#000',
            colorText: '#000',
            scaleWidth: 0.6,                // minimum value => 0.1
            scaleRate: 0.1,                 // minimum value => 0.1
            animate: true,
            timer: 20,
            multipleX: 1,                   // minimum value => 1
            multipleY: 1,                   // minimum value => 1
            resizeAuto: true,
            lineWidth: 2,                   // minimum value => 1
            circleSize: 3,                  // minimum value => 0
            typeGraph: TypeGraph.LINE,
            arround: 2,                     // minimum value => 0
            yScale: 10,                     // minimum value => 1
        },
    };
}

const timer = ms => new Promise(res => setTimeout(res, ms));
